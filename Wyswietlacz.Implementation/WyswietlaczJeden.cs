﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wyswietlacz.Contract;
using Lab5.DisplayForm;
using System.ComponentModel;
using System.Windows;


namespace Wyswietlacz.Implementation
{
    public class WyswietlaczJeden : IWyswietlacz
    {

        DisplayViewModel ViewModel;


        public WyswietlaczJeden()
        {
            ViewModel = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                // utworzenie nowej formatki graficznej stanowiącej widok
                var form = new Form();
                // utworzenie modelu widoku (wzorzec MVVM)
                var viewModel = new DisplayViewModel();
                // przypisanie modelu do widoku
                form.DataContext = viewModel;
                // wyświetlenie widoku
                form.Show();
                // zwrócenie modelu widoku do dalszych manipulacji
                return viewModel;
            }), null);
        }
        

        public void UtworzOkno(string Dane)
        {
            ViewModel.Text = Dane;
        }
    }
}
